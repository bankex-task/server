# Server

## Usage
Clone project:
```
git clone https://gitlab.com/bankex-task/server.git

cd server
```

Set environment variables:
```
source .env
```

Run server:
```
go run . server
```


Check endpoints:

```
curl http://localhost:3001/api/v1/rates?pairs=ETHBTC,LTCBTC

curl -X POST --data '{ "pairs": ["BTCUSDT", "ETHUSDT"] }' -H 'Content-type: application/json' http://localhost:3001/api/v1/rates
```




## That's all :)