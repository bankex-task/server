package handler

import (
	"github.com/labstack/echo/v4"
)

// RegisterRoutes to the route group
func RegisterRoutes(rg *echo.Group, handler *Handler) {
	rg.GET("/rates", handler.getRequestWrapper)
	rg.POST("/rates", handler.postRequestWrapper)
}
