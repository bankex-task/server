package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"

	"github.com/labstack/echo/v4"
	"gitlab.com/bankex-task/bankex-server/config"
)

// Handler handles client requests
type Handler struct {
	Config *config.Config
}

// InitHandler instantiate the handler
func InitHandler(conf *config.Config) *Handler {
	return &Handler{
		Config: conf,
	}
}

func (h *Handler) getRequestWrapper(c echo.Context) error {
	pairs := c.QueryParam("pairs")
	symbols := strings.Split(pairs, ",")
	rates, err := getRatesConcurrently(symbols, h.Config)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, Response(rates))
}

func (h *Handler) postRequestWrapper(c echo.Context) error {
	symbols := struct {
		Pairs []string `json:"pairs"`
	}{}
	err := c.Bind(&symbols)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	rates, err := getRatesConcurrently(symbols.Pairs, h.Config)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, Response(rates))
}

// ---------------------------------------------

type response map[string]string

func Response(c []*rate) response {
	var r = response{}
	for _, v := range c {
		r[v.Symbol] = v.Price
	}
	return r
}

func getRatesConcurrently(symbols []string, conf *config.Config) ([]*rate, error) {
	symbolsCount := len(symbols)
	var rates = make([]*rate, 0)
	var ch = make(chan *rateResponse, symbolsCount)
	var wg sync.WaitGroup

	// get rate for each symbol
	for _, symbol := range symbols {
		wg.Add(1)
		go getRateForSymbol(conf.RateSource.URL, symbol, &wg, ch)
	}
	wg.Wait()
	close(ch)

	for rate := range ch {
		if rate.Err != nil {
			return nil, rate.Err
		}
		rates = append(rates, rate.Result)
	}

	return rates, nil
}

func getRateForSymbol(url, symbol string, w *sync.WaitGroup, ch chan<- *rateResponse) {
	defer w.Done()

	req, err := http.NewRequest("GET", fmt.Sprintf("%s?symbol=%s", url, symbol), nil)
	if err != nil {
		ch <- &rateResponse{
			Result: nil,
			Err:    err,
		}
		return
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		ch <- &rateResponse{
			Result: nil,
			Err:    err,
		}
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		ch <- &rateResponse{
			Result: nil,
			Err:    err,
		}
		return
	}
	defer resp.Body.Close()

	result := &rate{}
	err = json.Unmarshal(body, result)
	if err != nil {
		ch <- &rateResponse{
			Result: nil,
			Err:    err,
		}
		return
	}

	ch <- &rateResponse{
		Result: result,
		Err:    nil,
	}
	return
}
