package handler

type rate struct {
	Symbol string `json:"symbol"`
	Price  string `json:"price"`
}

type rateResponse struct {
	Result *rate
	Err    error
}
