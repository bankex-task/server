package main

import (
	"context"
	"fmt"
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/bankex-task/bankex-server/config"
	"gitlab.com/bankex-task/bankex-server/handler"
	"go.uber.org/fx"
)

// NewServer runs http server and listens to given port
func NewServer(lc fx.Lifecycle, conf *config.Config) *echo.Echo {
	e := echo.New()
	lc.Append(fx.Hook{
		OnStart: func(context.Context) error {
			port := conf.Port
			log.Print(fmt.Sprintf("Listening at port %s...", port))
			go e.Start(fmt.Sprintf(":%s", port))
			return nil
		},
		OnStop: func(ctx context.Context) error {
			log.Print("Server is shutting down")
			return e.Shutdown(ctx)
		},
	})

	return e
}

// RegisterRoutes to the echo engine
func RegisterRoutes(e *echo.Echo, h *handler.Handler) {
	// route group for api v1
	api := e.Group("/api/v1")

	// register endpoints
	handler.RegisterRoutes(api, h)
}

func main() {
	app := fx.New(
		fx.Provide(
			config.InitConfig,
			NewServer,
			handler.InitHandler,
		),
		fx.Invoke(RegisterRoutes),
	)

	app.Run()
}
