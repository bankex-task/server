module gitlab.com/bankex-task/bankex-server

go 1.14

require (
	github.com/labstack/echo/v4 v4.2.1
	github.com/spf13/viper v1.7.1
	go.uber.org/fx v1.13.1
)
